-- To jest komentarz jednolinijkowy

/* 0. Tabela "Students" do wykorzystania w trakcie dzisiejszych zadań
	id name     sex date_of_birth exam_points
	1  Natalia  F   1960-02-03    98,06
	2  Karolina F   1999-11-02    59,27
	3  Tomasz   M   2000-01-01    45,13
	4  Kacper   M   1980-09-16    79,25 */


/* 1. Bazy danych
	a. Stwórz bazę danych "courses".
	b. Usuń bazę danych "courses".
	c. Ponownie usuń bazę danych "courses". (!)
	d. Jeszcze raz stwórz bazę danych "courses".
	e. Ponownie stwórz bazę danych "courses". (!) */

-- 1.a
CREATE DATABASE courses;

-- Jak użyć?
USE courses;

-- 1.b
DROP DATABASE courses;

-- 1.c
DROP DATABASE courses;

-- 1.d
CREATE DATABASE courses;

-- 1.e
CREATE DATABASE courses;

/* 2. Tabele
	a. Stwórz tabelę "students " zawierającą pięć kolumn:
		- id INTEGER, UNIQUE, NOT NULL --liczba całkowita, unikaty, pole niepuste
		- name TEXT
		- sex CHAR(1) --jeden znak (F - kobieta, M - mężczyzna)
		- date_of_birth DATE --data podana w formacie YYYY-MM-DD (rok-miesiąc-dzień)
		- exam_points DOUBLE --liczba niecałkowita
	b. Usuń tabelę „students”.
	c. Ponownie utwórz tabelę „students” jak w puncie 2a. */

-- 2.a.
CREATE TABLE students ( 
	id INTEGER UNIQUE NOT NULL,
	first_name TEXT,
	sex CHAR(1),
	date_of_birth DATE,
	exam_points FLOAT
);

-- 2.b
DROP TABLE students;

-- 2.c
CREATE TABLE students ( 
	id INTEGER UNIQUE NOT NULL,
	first_name TEXT,
	sex CHAR(1),
	date_of_birth DATE,
	exam_points FLOAT
);


/* 3. Dodawanie rekordów do tabeli (INSERT)
	a. Wstaw do tabeli "students" wiersz o id nr 1 z Tabeli 1.
	b. Wstaw do tabeli "students" wiersz o id nr 2 z Tabeli 1.
	c. Wstaw do tabeli "students" wiersz o id nr 3 z Tabeli 1.
	d. Wstaw do tabeli "students" wiersz o id nr 4 z Tabeli 1.
	e. Ponownie wstaw do tabeli "students" wiersz o id nr 4 z Tabeli 1. (!)
	f. Ponownie wstaw do tabeli "students" wiersz o id nr 4 z Tabeli 1 ale zmień nr id
	na nr 5.
	g. Wstaw dowolne dane do tabeli „students” ale nie wymieniaj nazw kolumn
	tabeli.
	h. Wstaw wiersz, który bêdzie miał wypełnione tylko dwie kolumny: id oraz imię.
	i. Wstaw wiersz, który bêdzie miał wypełnione tylko dwie kolumny id oraz imię –
	nie wymieniaj nazw kolumn tabeli. */

/*  id name     sex date_of_birth exam_points
	1  Natalia  F   1960-02-03    98,06
	2  Karolina F   1999-11-02    59,27
	3  Tomasz   M   2000-01-01    45,13
	4  Kacper   M   1980-09-16    79,25 */

-- 3.a-d
INSERT INTO students (id, first_name, sex, date_of_birth, exam_points)
VALUES (1, 'Natalia', 'F', '1960-02-03', 98.06);

INSERT INTO students (id, first_name, sex, date_of_birth, exam_points)
VALUES (2, 'Karolina', 'F', '1999-11-02', 59.27),
	   (3, 'Tomasz', 'M', '2000-01-01', 45.13),
	   (4, 'Kacper', 'M', '1980-09-16', 79.25);

-- 3.e
INSERT INTO students (id, first_name, sex, date_of_birth, exam_points)
VALUES (4, 'Kacper', 'M', '1980-09-16', 79.25);

-- 3.f
INSERT INTO students (id, first_name, sex, date_of_birth, exam_points)
VALUES (5, 'Kacper', 'M', '1980-09-16', 79.25);

-- 3.g Udało się, są wartości dla wszystkich kolumn
INSERT INTO students
VALUES (6, 'Agata', 'F', '2020-03-08', 0);

-- A jak jakiejś zapomnę?
INSERT INTO students
VALUES (7, 'Agata', '2020-03-08', 0);

-- A jak to naprawić?
INSERT INTO students (id, first_name, date_of_birth, exam_points)
VALUES (7, 'Agata', '2020-03-08', 0);

/* 4. Wyszukiwanie rekordów w tabeli (SELECT)
	a. Wyszukaj wszystkie rekordy znajdujące się w tabeli „students”. W zapytaniu
	użyj nazw wszystkich kolumn.
	b. Ponownie wyszukaj wszystkie rekordy znajdujące się w tabeli „students”. W
	zapytaniu pomiń nazwy kolumn. Zamiast nazw wstaw znak specjalny *.
	c. Wyszukaj tylko numer id oraz imiona wszystkich studentów w tabeli
	„students”.
	d. Wyszukaj daty urodzenia oraz imiona wszystkich studentów z tabeli „students”
	- zwróć uwagę na zmianę kolejności kolumn w zapytaniu. */

--4.a
SELECT id, first_name, sex, date_of_birth, exam_points
FROM students;

--4.b
SELECT *
FROM students;

--4.c
SELECT id, first_name
FROM students;

--4.d
SELECT date_of_birth, first_name
FROM students;

/* 5. Dokonywanie zmian w tabeli (ALTER)
	a. Do tabeli „students” dodaj kolumnę „age” przechowującą liczby całkowite.
	b. Zmodyfikuj typ przechowywanych danych przez kolumnê „age”. Od teraz
	będzie przechowywała daty.
	c. Usuń kolumnę „age” z tabeli „students”.
	d. Zmień nazwę tabeli „students” na „clouds”.
	e. Zmień nazwę tabeli „clouds” znowu na „students”. */

--5.a
ALTER TABLE students ADD age INTEGER;

--5.b
ALTER TABLE students ALTER COLUMN age FLOAT;

--5.c
ALTER TABLE students DROP COLUMN age;

SELECT * FROM students;

--5.d
-- Wersje dla MySQL/MariaDB
-- ALTER TABLE students RENAME clouds;

EXEC sp_rename 'students', 'clouds';

SELECT * FROM clouds;

EXEC sp_rename 'clouds', 'students';

-- A jak zmienić nazwę kolumny?
-- MySQL/MariaDB -> XAMPP
-- ALTER TABLE students RENAME COLUMN stara_nazwa TO nowa_nazwa

-- SQL Server
EXEC sp_rename 'students.first_name', 'name', 'COLUMN';

SELECT * FROM courses.dbo.students;

/* 6. Aktualizacja danych (UPDATE)
	a. Wybierz z tabeli „students” tylko tych studentów, którzy mają na imiê
	„Tomasz”.
	b. Wybierz z tabeli „students” tylko numery id studentów, którzy mają na imię
	„Tomasz”.
	c. Wybierz z tabeli „students” studenta o numerze id = 4.
	d. Zmień datę urodzenia studenta o numerze id = 2 na 1990-01-31.
	e. Student Kacper poprawił egzamin na 90 punktów. Dokonaj odpowiedniej
	zmiany w tabeli „students”.
	f. Studentka o id=1 tak naprawdę ma na imię ‘Roman’ i jest mężczyzną. Dokonaj
	odpowiednich zmian w tabeli „students”.
	g. Wszyscy studenci poprawili egzamin na 100 pkt. Dokonaj zmian w tabeli
	„students”. */

--6.a

ALTER TABLE students ALTER COLUMN name VARCHAR(255);

SELECT *
FROM students
WHERE name = 'Tomasz';

--6.b
SELECT id
FROM students
WHERE name = 'Tomasz';


--6.d
UPDATE students
SET date_of_birth = '1990-01-31'
WHERE id = 2;

SELECT * FROM students;

UPDATE students
SET date_of_birth = '1990-01-31';

SELECT * FROM students;

--6.f
UPDATE students
SET name = 'Roman',
    sex = 'M'
WHERE id = 1;


/* 7. Usuwanie danych z tabeli (DELETE)
	a. Usuń dane Igora z tabeli „students”.
	b. Usuń wszystkie rekordy z tabeli „students”. */

--7.a.
DELETE FROM students
WHERE name = 'Agata';

SELECT * FROM students;

--7.b
DELETE FROM students;

SELECT * FROM students;

/* 8. Ograniczenia (CONSTRAINTS)
	a. Stwórz tabelę "students2" zawierającą pięć kolumn (id INTEGER, name TEXT,
	sex CHAR(1), date_of_birth DATE, exam_points DOUBLE). Do kolumn dodaj
	ograniczenia:
		- id – jest to klucz główny, pole nie może być puste
		- name – nie może być puste,
		- sex – zawiera wyłącznie wartości F/M – należy użyć ograniczenia
		- date_of_birth – data nie może być większa lub równa od daty 2020-03-
		02
		- exam_points – jeżeli pole jest puste to wstaw wartość domyślną = 0. */

--8.a
CREATE TABLE students2 (
	id INTEGER UNIQUE NOT NULL,
	name TEXT NOT NULL,
	sex CHAR(1) CHECK(sex = 'F' OR sex = 'M'),
	date_of_birth DATE CHECK(date_of_birth < '2020-03-02'),
	exam_points FLOAT DEFAULT 0
);

INSERT INTO students2
VALUES (1, 'Marcin', 'M', '2021-03-08', 10);
